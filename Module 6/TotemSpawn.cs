﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TotemSpawn : MonoBehaviour
{
    public GameObject obstacle;

    GameObject[] agent;

    void Start()
    {
        agent = GameObject.FindGameObjectsWithTag("agent");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out hit))
            {
                Instantiate(obstacle, hit.point, obstacle.transform.rotation);
                foreach (GameObject a in agent)
                {
                    a.GetComponent<AIControl>().DetectNewTotem(hit.point);
                }
            }
        }
    }
}
