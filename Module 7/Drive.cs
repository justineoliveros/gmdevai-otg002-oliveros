﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drive : MonoBehaviour {

 	public float speed = 10.0F;
    public float rotationSpeed = 100.0F;
    Animator anim;
    public GameObject bullet;
    public GameObject turret;
    public float damage = 10f;
    public float hpMax = 100f;
    public float hp;

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Bullet")
        {
            takeDamage(damage);
        }
    }
    void fireBullets()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject b = Instantiate(bullet, turret.transform.position, turret.transform.rotation);
            b.GetComponent<Rigidbody>().AddForce(turret.transform.forward * 500);
        }
    }

    void OnEnable()
    {
        hp = hpMax;
    }

    public void takeDamage(float damage)
    {
        hp -= damage;

        if (hp <= 0f)
        {
            Destroy(gameObject);
        }
    }

    void Update() {
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);

        fireBullets();
    }
}
